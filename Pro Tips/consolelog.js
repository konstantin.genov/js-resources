const foo = { name: "Nikola", age: 23, fat: true };
const bar = { name: "Koce", age: 24, fat: false };
const baz = { name: "Stulbi", age: 20, fat: false };

console.log("%c Log:", "color: red; font-weight: bold; font-size: 18px;");
console.log({ foo, bar, baz });
console.table({ foo, bar, baz });
console.trace();

