const posts = [
  { title: "Post one", body: "This is post one" },
  { title: "Post two", body: "This is post two" },
];

function getPosts() {
  setTimeout(() => {
    let output = "";
    posts.forEach((post, index) => {
      output += `<li>${post.title}</li>`;
    });
    document.body.innerHTML = output; //insert elements into the DOM
  }, 1000);
}
// by introducing a callback function we can call getPosts synchronously
// otherwise if we call createPost and getPosts due to the timeout, getPosts will always be executed first
const createPost = (post, callback) => {
  setTimeout(() => {
    posts.push(post);
    callback();
  }, 2000);
};

// we pass in the getPosts function as the callback, but we DO NOT invoke it
createPost({ title: "Post Three", body: "This is post three" }, getPosts);
