const turtle = {
  name: "Bob",
  legs: 69,
  shell: true,
  type: "amphibious",
  meal: 10,
  diet: "berries",
};

const feed = animal => {
  return `Feed ${animal.name} ${animal.meal} ${animal.diet}`;
};

console.log(feed(turtle));

const feedDestructured = ({ name, meal, diet }) => {
  return `Feed ${name} ${meal} kilos of ${diet}`;
};

console.log(feedDestructured(turtle));
