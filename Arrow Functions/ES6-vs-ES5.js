function squareNumber(x){
    return x * x;
};
          //args are optional
const squareNumberArrow = (x) => x * 2; //brackets are optional 
                                        // return is optional

console.log(squareNumber(2));
console.log(squareNumberArrow(2));

function setup() {
    createCanvas(600, 400);
    background(0);
    let button = createButton('press');
    
    button.mousePressed(() => background(random(255)));

    // button.mousePressed(changeBackground);

    // function changeBackground(){
    //     background(random(255));
    // }
}

// Arrow functions remember the this context, whilst anonymous functions will 
// loose the scope and point us back to the global context