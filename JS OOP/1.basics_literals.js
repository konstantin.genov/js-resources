const s1 = "Hello";
console.log(typeof s1); // string

const s2 = new String("Hello");
console.log(typeof s2); //obj

/* Object Literals */
const book1 = {
  title: "Just a book",
  author: "Some guy",
  year: "1969",
  getSummary: function() {
    return `The book ${this.title} 
     was written by ${this.author} in ${this.year}`;
  },
};

/* 
https://blog.logrocket.com/anomalies-in-javascript-arrow-functions/
*/

const book2 = {
  title: "Just another book",
  author: "Some other guy",
  year: "1969",
  getSummary: () => {
    return `The book ${this.title} 
        was written by ${this.author} in ${this.year}`;
  },
};


// console.log(typeof book1.getSummary);
// console.log(book1.getSummary());

// console.log(Object.values(book2));
// console.log(Object.keys(book2));

// ES5 Vs ES6 when creating functions inside a variable
console.log(book1.getSummary.prototype);
console.log(book2.getSummary.prototype);


// ES5 functions call the constructor method from the prototype
// Allowing them to instantiate a 'this' context
// ES6 functions do not have a prototype, meaning 'this' will be undefined;

/* Unlike regular functions, arrow functions do not have a this binding of their own. 
The value of this is resolved to that of the closest non-arrow parent function or 
the global object otherwise. */