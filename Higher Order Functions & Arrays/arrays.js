const companies = [
  { name: "Company One", category: "Finance", start: 1981, end: 2003 },
  { name: "Company Two", category: "Retail", start: 1992, end: 2008 },
  { name: "Company Three", category: "Auto", start: 1999, end: 2007 },
  { name: "Company Four", category: "Retail", start: 1989, end: 2010 },
  { name: "Company Five", category: "Technology", start: 2009, end: 2014 },
  { name: "Company Six", category: "Finance", start: 1987, end: 2010 },
  { name: "Company Seven", category: "Auto", start: 1986, end: 1996 },
  { name: "Company Eight", category: "Technology", start: 2011, end: 2016 },
  { name: "Company Nine", category: "Retail", start: 1981, end: 1989 },
];
const ages = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32];

// forEach
// receives a callback function - it can take up to 3 things:
// - iterator: something we can use for each company
// - index of element
// - the entire array of objects
// (company, index, companies) => {...}
companies.forEach(company => {
  console.log(company);
});

// filter takes in a function
const canDrink = ages.filter(age => {
  if (age >= 21) {
    return true;
  }
});

const shorterCanDrink = ages.filter(age => age >= 21);

console.log(canDrink);
console.log(shorterCanDrink);

const retailCompanies = companies.filter(
  company => company.category === "Retail"
);

console.log(retailCompanies);

const eightiesCompanies = companies.filter(
  company => company.start >= 1980 && company.start <= 1990
);
console.log(eightiesCompanies);

const lastedTenYears = companies.filter(
  company => company.end - company.start > 10
);

console.log(JSON.stringify(lastedTenYears));

// map
// this method allows us to create an array and influence its values
// like we have an array of objects (companies), we can create an array containing company names only:
const companyNames = companies.map(company => company.name);
console.log(companyNames);

const agesSquared = ages.map(age => Math.sqrt(age));
console.log(agesSquared);

// sort
// reduce
