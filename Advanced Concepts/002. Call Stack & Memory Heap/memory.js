// Call stack + Memory heap


// Memory heap is a large region in memory that is provided to us
// by the JS engine. Stored in no order.
const number = 610; //allocate memory for number
const string = 'some text'; //allocate memory for string
const human = { // allocate memory for an object and its values
    first: 'Konstantin',
    last: 'Genov'
}

// function allocated in memory
function calculate() {
    const sumTotal = 4 + 5;
    return subtractTwo(sumTotal);
}

function subtractTwo(num){
    return num - 2;
}

calculate();  // we use the callstack 
subtractTwo(10);

// Callstack 

anonymous() -> calculate() -> subtractTwo() -> calculate() -> anonymous() 