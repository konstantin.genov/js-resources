const posts = [
  { title: "Post one", body: "This is post one" },
  { title: "Post two", body: "This is post two" },
];

function getPosts() {
  setTimeout(() => {
    let output = "";
    posts.forEach((post, index) => {
      output += `<li>${post.title}</li>`;
    });
    document.body.innerHTML = output; //insert elements into the DOM
  }, 1000);
}
// by using a Promise we can synchronously execute a function by then chaining it 
// to the result of the promise (then syntax)
const createPost = post => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      posts.push(post);

      const error = true;
      if (!error) {
        resolve();
      } else {
        reject("Error: Something went wrong");
      }
    }, 2000);
  });
};

// we no longer pass a callback like getPosts, now we can use the then syntax
createPost({ title: "Post Three", body: "This is post three" }).then(getPosts).catch(e => {
    alert(e);
});
