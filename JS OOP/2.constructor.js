// Constructor

function Book(title, author, year) {
  this.title = title;
  this.author = author;
  this.year = year;

  this.getSummary = () => {
    return `The book ${this.title} 
     was written by ${this.author} in ${this.year}`;
  };
}

const book1 = new Book("Book", "Jane Doe", "1995");
const book2 = new Book("Book Two", "John Doe", "1923");

console.log(typeof book1);

console.log(book1.getSummary());
