// Classes

class Book {
  constructor(title, author, year) {
    this.title = title;
    this.author = author;
    this.year = year;
  }

  // behind the scenes, the function will be added to the prototype of Book
  getAge() {
    const years = new Date().getFullYear() - this.year;
    return `${this.title} is ${years} years old.`;
  }
  getSummary() {
    return `The book ${this.title} 
    was written by ${this.author} in ${this.year}`;
  }
}

// Instantiate

const book1 = new Book("Book title", "Some guy", 1995);

console.log(book1);
console.log(book1.getAge());
console.log(book1.getSummary());
