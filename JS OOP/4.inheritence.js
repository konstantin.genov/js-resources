// Inheritance

function Book(title, author, year) {
  this.title = title;
  this.author = author;
  this.year = year;
}

Book.prototype.getSummary = function() {
  return `The book ${this.title} 
      was written by ${this.author} in ${this.year}`;
};

// Magazine Constructor

function Magazine(title, author, year, month) {
  Book.call(this, title, author, year);
  this.month = month;
}

// Inherit prototype 
Magazine.prototype = Object.create(Book.prototype);

const mag1 = new Magazine('The book of books', 'J. Stalin', 1995, 'May');

console.log(mag1);
console.log(mag1.getSummary());
