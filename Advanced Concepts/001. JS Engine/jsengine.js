// Javascript Engine
 
/* The V8 engine was created by Google to solve the issues with Google maps. 
As engines before that were running JS a bit slow, all the previous engines made 
Google maps pretty slow. (Written in C++) */

/* Firefox uses the SpiderMonkey engine
   Edge uses Chakra */

/* ECMAScript is the governing body of JS that essentially decides how the language
should be standardized. */

/* In programming there are generally two ways of translating to machine language 
or something that our computers can understand. 

1. Interpreter:
 - Translates and reads the files line by line on the fly. 
 - Initially that's how JS worked.

2. Compiler:
 - The compiler, unlike the interpreter translates ahead of time to create a translation
 of what code we've just written and it compiles it down to usually a language that
 can be understood by our machines. It doesn't look at our code one by line.

 ** The interpreter converts our code to byte code
 ** The compiler converts our code to machine code */

 /* JIT (just in time) compiler in the V8 engine. 
 Our code is first run through the interpreter and is being watched by a Profiler.
 The profiler watches and monitors our code as it runs and makes notes on
 how we can optimize our code. Using this Profiler as our code is running through
 our interpreter, if it sees that the same line of code is being run a few times,
 the code is passed of to the compiler or JIT compiler. Then, the compiler replaces
 section of our code with an optimized version and is used from that point instead 
 of the slower byte code. */



 /* What is the difference between machine code and bytecode ?
 The main difference between machine code and bytecode 
 is that the machine code is a set of instructions in machine language or binary 
 that can be directly executed by the CPU while the bytecode is an intermediate code
generated from compiling a source code which can be executed by a virtual machine. 

In Java, The JVM converts the bytecode into machine code. Any computer with a JVM can 
execute that bytecode. In other words, any platform that consists of a JVM 
can execute a Java Bytecode. Machine code is a computer programming language consisting
 of binary instructions which a computer can respond to directly. 
 In contrast, bytecode is a form of instruction set designed for efficient execution 
 by a software such as a virtual machine. Hence, this explains the fundamental difference
  between machine code and bytecode. */